#!/usr/bin/pytyon3

# Exposes a EDF + Logfile as a HDF5/Nexus-like interface

from . import runlog
from . import edf
